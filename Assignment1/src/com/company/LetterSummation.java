package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LetterSummation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputString = scanner.next();
        Map<Character, Integer> simpleLetterMap = new HashMap<>();
        Map<Character, Integer> capitalLetterMap = new HashMap<>();
        int simpleIncrement = 1;
        int capitalIncrement = 1;
        for(char i='a';i<='z';i++){
            simpleLetterMap.put(i,simpleIncrement++);
        }for(char i='A';i<='Z';i++){
            capitalLetterMap.put(i,capitalIncrement++);
        }

        int letterSummation=0;
        for(char character:inputString.toCharArray()){
            if(simpleLetterMap.containsKey(character)){
                letterSummation = letterSummation+ simpleLetterMap.get(character);
            }if(capitalLetterMap.containsKey(character)){
                letterSummation = letterSummation+ capitalLetterMap.get(character);
            }
        }
        System.out.println("The letter value is "+letterSummation);

        Scanner scannerTest = new Scanner(System.in);
        System.out.println("Enter a name here: ");
        String str = scannerTest.nextLine();
        int sum = 0;
        for (char ch : str.toCharArray()) {
            if (ch >= 'A' && ch <= 'Z') {
                sum += 1 + ch - 'A';
                System.out.println(ch-'A');
            }if (ch >= 'a' && ch <= 'z') {
                sum += 1 + ch - 'a';
                System.out.println(ch-'a');
            }
        }
        System.out.printf("The sum of %s is %d%n", str, sum);
    }
}

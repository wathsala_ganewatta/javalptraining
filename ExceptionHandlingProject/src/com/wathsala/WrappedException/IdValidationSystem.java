package com.wathsala.WrappedException;

import com.wathsala.BankAccountSystem;
import com.wathsala.exception.IDNotMatchWithRegExException;
import com.wathsala.exception.IdNotValidException;

import java.util.Scanner;

public class IdValidationSystem {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        BankAccountSystem account = new BankAccountSystem();
        System.out.println("Enter your id ");
        String id = scanner.next();


        try{

            if(!(id.trim().matches("^[0-9]{9}[vVxX]$")))
            {
                throw new IDNotMatchWithRegExException("regEx not match");
            }

        }
        catch(IDNotMatchWithRegExException ex){
            throw new IdNotValidException(ex);
        }
    }
}

package com.wathsala.WrappedException;

import com.wathsala.AccountNumber;
import com.wathsala.BankAccountSystem;
import com.wathsala.Validation;
import com.wathsala.exception.AccountNumberNotFoundException;
import com.wathsala.exception.AccountNumberNotValidException;

import java.util.Scanner;

public class AccountNumberValidationSystem {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        BankAccountSystem account = new BankAccountSystem();
        Validation validation = new Validation();
        System.out.println("Enter your Account Number ");
        String accountNo = scanner.next();


        try
        {
            AccountNumber accountNumber = new AccountNumber();
            accountNumber.findAccountNumber(accountNo);
        }
        catch( AccountNumberNotFoundException ex) // catch a Checked Exception
        {
            throw new AccountNumberNotValidException("Account number is not valid",ex); // throw new Runtime Exception
        }

    }
}

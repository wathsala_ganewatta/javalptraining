package com.wathsala;

import com.wathsala.exception.AccountNumberNotFoundException;
import com.wathsala.exception.AccountNumberNotValidException;

public class Validation {
    public void accountNumberValidation(String accountNo) throws AccountNumberNotValidException {

        try
        {
            AccountNumber accountNumber = new AccountNumber();
            accountNumber.findAccountNumber(accountNo);
        }
        catch( AccountNumberNotFoundException ex) // catch a Checked Exception
        {
            throw new AccountNumberNotValidException("Account number is not valid",ex); // throw new Runtime Exception
        }

    }
}

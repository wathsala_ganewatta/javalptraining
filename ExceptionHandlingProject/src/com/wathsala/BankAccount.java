package com.wathsala;

import com.wathsala.exception.AccountNotFoundException;
import com.wathsala.exception.AccountNumberNotValidException;

public class BankAccount {
    void accountMatching(String accountNo) throws AccountNotFoundException {
        try{
            Validation validation = new Validation();
            validation.accountNumberValidation(accountNo);

        }
        catch( AccountNumberNotValidException ex)   //catch a runtime Exception
        {
            throw new AccountNotFoundException ("Account is not found",ex); // throw checked Exception
        }

    }
}

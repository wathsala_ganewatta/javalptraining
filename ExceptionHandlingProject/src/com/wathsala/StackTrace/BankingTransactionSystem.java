package com.wathsala.StackTrace;

import com.wathsala.BankAccount;
import com.wathsala.BankAccountSystem;
import com.wathsala.Transaction;
import com.wathsala.exception.AmountNotValidException;
import com.wathsala.exception.InsufficientFundException;
import com.wathsala.exception.TransactionNotValidException;

import java.util.InputMismatchException;
import java.util.Scanner;
public class BankingTransactionSystem {

    private void transaction(BankAccountSystem account, Scanner scanner){
        System.out.println("Select option:\n\t 1.withdraw\n\t 2.deposit \n\t 3.Check balance\n\t 4.Exit");
        int option= scanner.nextInt();
        int amount ;
        switch(option)
        {
            case 1:System.out.println("Enter amount to withdraw :");
                try{
                    amount = scanner.nextInt();
                    account.withdraw(amount);
                    System.out.println("You have Successfully withdraw "+amount);
                    System.out.println("Your Current balance is :" + account.getBalance()+"\n\n");
                    System.out.println("Continue?(y/n)");
                    char response = scanner.next().charAt(0);
                    if(response=='y')
                        transaction(account,scanner);
                    else if(response == 'n')
                        break;



                }
                catch(InputMismatchException ex){
                    System.out.print("you didn't enter a number value ");
                }
                catch(InsufficientFundException ex)
                {
                    ex.printStackTrace();
                }
                break;
            case 2:System.out.println("Enter amount to deposit:");
                try{
                    amount = scanner.nextInt();
                    account.deposit(amount);
                    System.out.println("You have Successfully deposit "+amount);
                    System.out.println("Your Current balance is :" + account.getBalance()+"\n\n");
                    System.out.println("Continue?(y/n)");
                    char response = scanner.next().charAt(0);
                    if(response=='y')
                        transaction(account,scanner);
                    else if(response == 'n')
                        break;

                }
                catch(InputMismatchException ex){
                    System.out.print("you didn't enter a number value ");
                }
                catch(AmountNotValidException ex)
                {
                    ex.printStackTrace();
                }
                break;
            case 3:System.out.println("Your Current balance is :" + account.getBalance()+"\n\n");
                transaction(account,scanner);
                break;
            case 4:System.out.println("Thank you for Connecting with the java banking system");
                break;
            default:transaction(account,scanner);


        }


    }
    public static void main(String[] args){
        BankingTransactionSystem exceptionHandling = new BankingTransactionSystem();
        Transaction transaction = new Transaction();
        BankAccountSystem bankAccountSystem = new BankAccountSystem();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Your Current Account balance is "+ bankAccountSystem.getBalance());
        System.out.println("Your account number is 7894561528");
        System.out.println("Enter your Account Number ");
        String accountNo = scanner.next();



        try{
            transaction.findAccountDetails(accountNo);
            exceptionHandling.transaction(bankAccountSystem,scanner);
        }
        catch(TransactionNotValidException ex) // catch an Exception
        {
            ex.printStackTrace();
        }






    }

}

package com.wathsala.exception;

public class IdNotValidException extends RuntimeException {
    public IdNotValidException(String message, Throwable exception)
    {
        super(message,exception);
    }
    public IdNotValidException (Throwable exception)
    {
        super(exception);
    }
}

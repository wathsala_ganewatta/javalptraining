package com.wathsala.exception;

public class InsufficientLengthOfAccountNumberException extends RuntimeException{
    public InsufficientLengthOfAccountNumberException(String message){
        super(message);
    }

}

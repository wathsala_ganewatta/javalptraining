package com.wathsala.exception;

public class IDNotMatchWithRegExException extends RuntimeException{
    public IDNotMatchWithRegExException(String message){
        super(message);
    }

}

package com.wathsala.exception;

public class AccountNotFoundException extends Exception{
    public AccountNotFoundException(String message, Throwable ex)
    {
        super(message,ex);
    }
}

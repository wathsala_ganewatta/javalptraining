package com.wathsala.exception;

public class AccountNumberNotValidException extends RuntimeException {
    public AccountNumberNotValidException(String message, Throwable exception)
    {
        super(message,exception);
    }
    public AccountNumberNotValidException(Throwable exception)
    {
        super(exception);
    }
}

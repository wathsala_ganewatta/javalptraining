package com.wathsala.exception;

public class TransactionNotValidException extends Exception  {
    public TransactionNotValidException(String message, Throwable exception)
    {
        super(message, exception);
    }
}

package com.wathsala.exception;

public class AccountNumberNotFoundException extends Exception{
    public AccountNumberNotFoundException(String message)
    {
        super(message);
    }
}

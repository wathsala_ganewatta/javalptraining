package com.wathsala.exception;

public class IDNotFoundException extends Exception{
    public IDNotFoundException(String message)
    {
        super(message);
    }
}

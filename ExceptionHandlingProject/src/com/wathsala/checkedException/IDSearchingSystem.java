package com.wathsala.checkedException;

import com.wathsala.BankAccountSystem;
import com.wathsala.exception.IDNotFoundException;

import java.util.Scanner;

public class IDSearchingSystem {
    public static void main(String[] args)
    {

        BankAccountSystem account = new BankAccountSystem();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your id ");


        String id = scanner.next();
        try{
            account.findId(id);
        }
        catch( IDNotFoundException ex)
        {
            ex.printStackTrace();
        }
    }
}

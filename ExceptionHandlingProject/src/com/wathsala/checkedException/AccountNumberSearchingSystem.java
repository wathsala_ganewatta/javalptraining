package com.wathsala.checkedException;

import com.wathsala.AccountNumber;
import com.wathsala.BankAccountSystem;
import com.wathsala.exception.AccountNumberNotFoundException;

import java.util.Scanner;

public class AccountNumberSearchingSystem {
    public static void main(String[] args)
    {

        BankAccountSystem account = new BankAccountSystem();
        AccountNumber accountNumber = new AccountNumber();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your Account Number ");


        String accountNo = scanner.next();
        try{
            accountNumber.findAccountNumber(accountNo);
        }
        catch( AccountNumberNotFoundException ex) //catch checked exception
        {
            System.err.print(ex);
        }
    }
}

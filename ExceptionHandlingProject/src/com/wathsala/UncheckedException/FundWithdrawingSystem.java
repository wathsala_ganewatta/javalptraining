package com.wathsala.UncheckedException;

import com.wathsala.BankAccountSystem;
import com.wathsala.exception.InsufficientFundException;

import java.util.Scanner;

public class FundWithdrawingSystem {
    public static void main(String[] args){

        BankAccountSystem bankAccountSystem = new BankAccountSystem();
        System.out.println("Account Balance is "+ bankAccountSystem.getBalance());
        System.out.println("Enter amount to withdraw");
        Scanner scanner = new Scanner(System.in);
        int amount = scanner.nextInt();
        try{
            bankAccountSystem.withdraw(amount);
        }
        catch(InsufficientFundException ex)       //catch a Runtime Exception
        {
            System.err.print(ex);
        }
    }
}

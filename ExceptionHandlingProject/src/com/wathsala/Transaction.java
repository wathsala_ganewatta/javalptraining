package com.wathsala;

import com.wathsala.exception.AccountNotFoundException;
import com.wathsala.exception.TransactionNotValidException;

public class Transaction {
    public void findAccountDetails(String accountNo) throws TransactionNotValidException {
        try{
            BankAccount bankAccount = new BankAccount();
            bankAccount.accountMatching(accountNo);
        }
        catch(AccountNotFoundException ex)	//catch checked Exception
        {
            throw new TransactionNotValidException("Transaction is not valid" , ex); //throw checked exception
        }

    }
}

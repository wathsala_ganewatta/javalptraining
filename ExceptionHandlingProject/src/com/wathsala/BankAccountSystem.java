package com.wathsala;

import com.wathsala.exception.*;

public class BankAccountSystem {
    private int balance = 3000;

    public int getBalance(){
        return balance;
    }

    public void deposit(int amount) throws AmountNotValidException {

        if(amount>0){

            balance = balance + amount;
        }
        else{
            throw new AmountNotValidException ("Amount not valid to deposit :: RuntimeException Handling"); // throw runtime exception
        }


    }


    public void withdraw(int amount) throws InsufficientFundException {
        if(amount>balance){
            throw new InsufficientFundException ("Insufficient balance for withdraw :: Runtime Exception Handling"); // throw custom exception
        }
        balance-=amount;

    }








}

package com.wathsala;

public class BubbleSortUsingWhileAndForLoop {

    public static void main(String[] args) {
        int[] array = {3,5,3,1,3,4,6};
        boolean sorted = false;
        int temp;
        for (int value : array) {
            System.out.print(value+ " ");
        }
        while(!sorted){
            sorted = true;
            for(int i=0;i<array.length-1;i++){
                if(array[i]>array[i+1]){
                    temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    sorted = false;
                }
            }
        }
        System.out.println();
        for (int value : array) {
            System.out.print(value + " ");
        }
    }
}

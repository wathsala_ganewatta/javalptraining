package com.wathsala;

public class BubbleSortUsingTwoFourLoop {
    public static void main(String[] args) {
        int[] array = {3,5,3,1,3,4,6,1};
        for (int value : array) {
            System.out.print(value+ " ");
        }
        for(int i= 0;i<array.length-1;i++){
            for(int j=i+1;j<array.length;j++){
                if(array[i]>array[j]){
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println();
        for (int value : array) {
            System.out.print(value + " ");
        }
    }
}

package com.wathsala;

public class InsertionSort {

    public static void main(String[] args) {
        int[] array = {2,4,6,2,4,8,5,1};
        System.out.println("Before Sorting");
        for (int value:array) {
            System.out.print(value +" ");

        }
        for(int i=1;i<array.length;i++){
            int current = array[i];
            int j = i-1;
            while(j>=0 && current <array[j]){
                array[j+1] = array[j];
                j--;
            }
            array[j+1] = current;
        }
        System.out.println("\nAfter Adding Insertion Sort");

        for (int value:array) {
            System.out.print(value +" ");

        }
    }
}
